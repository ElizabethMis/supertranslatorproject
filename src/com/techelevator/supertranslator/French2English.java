package com.techelevator.supertranslator;

import java.util.HashMap;
import java.util.Map;

public class French2English {

	public static void main(String[] args) {
		
		String french = args[0].toLowerCase();
		
		Map<String, String> fren2eng = new HashMap<String, String>();
		fren2eng.put("un", "One");
		fren2eng.put("deux", "Two");
		fren2eng.put("trois", "Three");
		fren2eng.put("quatre", "Four");
		fren2eng.put("cinq", "Five");
		fren2eng.put("six", "Six");
		fren2eng.put("sept", "Seven");
		fren2eng.put("huit", "Eight");
		fren2eng.put("neuf", "Nine");
		fren2eng.put("dix", "Ten");
		
		String english = fren2eng.get(french);
		
		if (english != null) {
			System.out.println("\n****Your translation was succsessful!****\n");
			System.out.println("          " + args[0] + " " + "in english is" + " " + english);
			System.out.println("\n*****************************************\n");
		}else {
			System.out.println("I am sorry, I don't speak french very well :(");
		}
	}
}
