package com.techelevator.supertranslator;

import java.util.HashMap;
import java.util.Map;

public class English2French {

	public static void main(String[] args) {
		String english = args[0].toLowerCase();
		
		Map<String, String> eng2fren = new HashMap<String, String>();
		eng2fren.put("one", "Un");
		eng2fren.put("two", "Deux");
		eng2fren.put("three", "Trois");
		eng2fren.put("four", "Quatre");
		eng2fren.put("five", "Cinq");
		eng2fren.put("six", "Six");
		eng2fren.put("seven", "Sept");
		eng2fren.put("eight", "Huit");
		eng2fren.put("nine", "Neuf");
		eng2fren.put("ten", "Dix");
		
		String french = eng2fren.get(english);
		
		if(french != null) {
			System.out.println(french);
		} else {
			System.out.println("I am sorry, I don't know that word :(");
		}
			
	}

}
