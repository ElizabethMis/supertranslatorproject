package com.techelevator.supertranslator;

import java.util.HashMap;
import java.util.Map;

public class Spanish2English {

	public static void main(String[] args) {

		String spanish = args[0].toLowerCase();
		
		Map<String, String> esp2eng = new HashMap<String, String>();
		esp2eng.put("uno", "One");
		esp2eng.put("dos", "Two");
		esp2eng.put("tres", "Three");
		esp2eng.put("cuatro", "Four");
		esp2eng.put("cinco", "Five");
		esp2eng.put("seis", "Six");
		esp2eng.put("siete", "Seven");
		esp2eng.put("ocho", "Eight");
		esp2eng.put("nueve", "Nine");
		esp2eng.put("diez", "Ten");
		
		String english = esp2eng.get(spanish);
		
		if(english != null) {
			System.out.println(args[0] + " " + "in english is" + " " + english);
		} else {
			System.out.println("I am sorry, I don't know that word :(");
		}
		
		
		
	}

}
