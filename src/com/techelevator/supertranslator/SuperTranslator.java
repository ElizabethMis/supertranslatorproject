package com.techelevator.supertranslator;

public class SuperTranslator {

	public static void main(String[] args) {
	
		String trans = args[0].toLowerCase();
		String[] word = {args[1]};
		
		if (trans.equals("e2s")){
			English2Spanish.main(word);
		} else if (trans.equals("s2e")){
			Spanish2English.main(word);
		} else if (trans.equals("e2f")){
			English2French.main(word);
		} else if (trans.equals("f2e")){
			French2English.main(word);
		} 
	}
}
